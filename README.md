# Sample-app

## Pipeline

[![build](https://gitlab.com/makyntoch/sample-app/badges/master/pipeline.svg)](https://gitlab.com/makyntoch/ci-app/commits/master)

## Coverage

[![coverage](https://gitlab.com/makyntoch/sample-app/badges/master/coverage.svg)](https://gitlab.com/makyntoch/ci-app/commits/master)
